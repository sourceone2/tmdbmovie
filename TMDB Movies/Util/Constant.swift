//
//  Constant.swift
//  TMDB Movies
//
//  Created by Navindas Ghadge on 14/03/23.
//

import Foundation

final class Constant {
    private init() {}
    
    
    
    static public let sharedInstance: Constant = {
        let instance = Constant()
        return instance
    }()
    
    
    private static let baseURLString: String = "https://api.themoviedb.org/3/movie/"
    private static let baseImageURLString: String = "https://image.tmdb.org/t/p/"
    
    let popularMovieURL: String = baseURLString + "popular"
    let latestMovieURL: String = baseURLString + "latest"
    let backDropImageURL: String = baseImageURLString + "w500"
    let posterImageURL: String = baseImageURLString + "original"
    
    let apiKey: String = "909594533c98883408adef5d56143539"
    
    
}
