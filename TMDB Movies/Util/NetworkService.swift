//
//  NetworkService.swift
//  TMDB Movies
//
//  Created by Navindas Ghadge on 14/03/23.
//

import Foundation
import SystemConfiguration

enum EndResult {
    case success(Data)
    case failure(String)
}


final class NetworkService {
    static func isConnected() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    private init(){}
    
    static func getData(urlString: String, completion: @escaping (_ result: EndResult) -> Void) {
        var urlRequest = URLRequest(url: URL(string: urlString)!)
        urlRequest.httpMethod = "GET"
        
        let session = URLSession(configuration: .default)
        if NetworkService.isConnected() {
            session.dataTask(with: urlRequest) { receivedData, receivedResponse, receivedError in
                guard receivedError == nil else {
                    print(receivedError!.localizedDescription)
                    completion(.failure(receivedError!.localizedDescription))
                    return
                }
                
                guard let data = receivedData else {
                    completion(.failure("Something went wrong"))
                    return
                }
                
                guard let response = receivedResponse else {
                    return
                }
                print(response)
//                print(String(data: data, encoding: .utf8)!)
                completion(.success(data))
            }.resume()
        } else {
            completion(.failure("Please connect to internet."))
        }
    }
}
