//
//  Extension.swift
//  TMDB Movies
//
//  Created by Navindas Ghadge on 15/03/23.
//

import Foundation
import UIKit

extension UIViewController {
    func showIndicator() {
        DispatchQueue.main.async {
            let widthHeight = 50.0
            let activityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 0.0, y: 0.0, width: widthHeight, height: widthHeight))
            activityIndicatorView.center = self.view.center
            activityIndicatorView.layer.cornerRadius = 10.0
            if #available(iOS 13.0, *) {
                activityIndicatorView.backgroundColor = .systemGray6
            } else {
                // Fallback on earlier versions
                activityIndicatorView.backgroundColor = .lightGray
            }
            if #available(iOS 13.0, *) {
                activityIndicatorView.style = .large
            } else {
                // Fallback on earlier versions
                activityIndicatorView.style = .whiteLarge
            }
            activityIndicatorView.color = .cyan
            self.view.addSubview(activityIndicatorView)
            activityIndicatorView.startAnimating()
        }
    }
    
    func hideIndicator() {
        DispatchQueue.main.async {
            for currentView in self.view.subviews {
                if currentView.isKind(of: UIActivityIndicatorView.self) {
                    (currentView as? UIActivityIndicatorView)?.stopAnimating()
                    currentView.removeFromSuperview()
                }
            }
        }
    }
    
}
