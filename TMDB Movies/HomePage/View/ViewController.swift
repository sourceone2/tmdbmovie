//
//  ViewController.swift
//  TMDB Movies
//
//  Created by Navindas Ghadge on 14/03/23.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: MovieListViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.setupViewModel()
        self.setupTableView()
    }
    
    
    //MARK: - Local methods
    private func setupTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.register(UINib(nibName: "MovieTVC", bundle: nil), forCellReuseIdentifier: "MovieTVC")
    }
    
    private func setupViewModel() {
        self.viewModel = MovieListViewModel(delegate: self, movieDataSource: MovieListDataSource())
        self.viewModel.getLatestMovie()
    }
    
    @IBAction func refreshData(_ sender: UIBarButtonItem) {
        self.viewModel.getLatestMovie()
    }
    
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.latestMovieModel != nil ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "MovieTVC", for: indexPath) as! MovieTVC
        
        let title = self.viewModel.latestMovieModel.original_title ?? ""
        let voteAvg: Double = self.viewModel.latestMovieModel.vote_average ?? 0.0
        let voteCnt: Int = self.viewModel.latestMovieModel.vote_count ?? 0
        let url = self.viewModel.latestMovieModel.backdrop_path ?? ""
        cell.updateCell(title: title, voteAvg: voteAvg, voteCount: voteCnt,backdropUrl: url)
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let screenSize = view.window?.windowScene?.screen.bounds.size {
            return (screenSize.width - 20.0) * 0.562
        } else {
            return (UIScreen.main.bounds.size.width - 20) * 0.562
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movieDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "MovieDetailVC") as! MovieDetailVC
        movieDetailVC.movieTitle = self.viewModel.latestMovieModel.original_title ?? ""
        movieDetailVC.movieOverview = self.viewModel.latestMovieModel.overview ?? ""
        movieDetailVC.backdropUrl = self.viewModel.latestMovieModel.poster_path ?? ""
        self.navigationController?.pushViewController(movieDetailVC, animated: true)
    }
}

extension ViewController: ViewModelDelegate {
    func willLoadData() {
        self.showIndicator()
    }
    
    func didLoadData() {
        DispatchQueue.main.async {
            self.hideIndicator()
            self.tableView.reloadData()
        }
    }
    
    func didFail(err: String) {
        DispatchQueue.main.async {
            self.hideIndicator()
        }
    }
}
