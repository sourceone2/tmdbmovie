//
//  PopularVC.swift
//  TMDB Movies
//
//  Created by Navindas Ghadge on 14/03/23.
//

import UIKit

class PopularVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: MovieListViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupViewModel()
        self.setupTableView()
    }
    
    //MARK: - Local methods
    private func setupTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.register(UINib(nibName: "MovieTVC", bundle: nil), forCellReuseIdentifier: "MovieTVC")
    }
    
    private func setupViewModel() {
        self.viewModel = MovieListViewModel(delegate: self, movieDataSource: MovieListDataSource())
        self.viewModel.getPopularMovie()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PopularVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.popularMovieModel != nil ? self.viewModel.popularMovieModel.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "MovieTVC", for: indexPath) as! MovieTVC
        
        let title = self.viewModel.popularMovieModel[indexPath.row].original_title ?? ""
        let voteAvg: Double = self.viewModel.popularMovieModel[indexPath.row].vote_average ?? 0.0
        let voteCnt: Int = self.viewModel.popularMovieModel[indexPath.row].vote_count ?? 0
        let url = self.viewModel.popularMovieModel[indexPath.row].backdrop_path ?? ""
        cell.updateCell(title: title, voteAvg: voteAvg, voteCount: voteCnt,backdropUrl: url)
        return cell
    }
}

extension PopularVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let screenSize = view.window?.windowScene?.screen.bounds.size {
            return (screenSize.width - 20.0) * 0.562
        } else {
            return (UIScreen.main.bounds.size.width - 20) * 0.562
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.viewModel.popularMovieModel.count > 0 {
            if indexPath.row == self.viewModel.popularMovieModel.count - 2 {
                self.viewModel.getPopularMovie()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movieDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "MovieDetailVC") as! MovieDetailVC
        movieDetailVC.movieTitle = self.viewModel.popularMovieModel[indexPath.row].original_title ?? ""
        movieDetailVC.movieOverview = self.viewModel.popularMovieModel[indexPath.row].overview ?? ""
        movieDetailVC.backdropUrl = self.viewModel.popularMovieModel[indexPath.row].poster_path ?? ""
        self.navigationController?.pushViewController(movieDetailVC, animated: true)
    }
}

extension PopularVC: ViewModelDelegate {
    func willLoadData() {
        self.showIndicator()
    }
    
    func didLoadData() {
        DispatchQueue.main.async {
            self.hideIndicator()
            
            var indexrow : [IndexPath] = []
            let maxRows = self.tableView.numberOfRows(inSection: 0)
            
            if maxRows == 0 {
                self.tableView.reloadData()
            } else {
                for i in maxRows...(maxRows + 19) {
                    let indexPath = IndexPath(row: i, section: 0)
                    indexrow.append(indexPath)
                }
                
//                self.tableView.beginUpdates()
                self.tableView.performBatchUpdates {
                    self.tableView.insertRows(at: indexrow, with: .bottom)
                }
//                self.tableView.endUpdates()
                
            }
            
            
            
        }
    }
    
    func didFail(err: String) {
        DispatchQueue.main.async {
            self.hideIndicator()
        }
    }
}
