//
//  MovieDetailVC.swift
//  TMDB Movies
//
//  Created by Navindas Ghadge on 15/03/23.
//

import UIKit

class MovieDetailVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var backdropUrl: String?
    var movieTitle: String?
    var movieOverview: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupTableView()
    }
    
    func setupTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.register(UINib(nibName: "MovieDetailTVC", bundle: nil), forCellReuseIdentifier: "MovieDetailTVC")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MovieDetailVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "MovieDetailTVC", for: indexPath) as! MovieDetailTVC
        cell.updateCell(title: movieTitle!, overview: movieOverview!, posterUrl: backdropUrl!)
        return cell
    }
}

extension MovieDetailVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let screenSize = view.window?.windowScene?.screen.bounds.size {
            return screenSize.width * 1.5
        } else {
            return UIScreen.main.bounds.size.width * 1.5
        }
    }
}
