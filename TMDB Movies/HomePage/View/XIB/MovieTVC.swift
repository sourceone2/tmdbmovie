//
//  MovieTVC.swift
//  TMDB Movies
//
//  Created by Navindas Ghadge on 15/03/23.
//

import UIKit

class MovieTVC: UITableViewCell {
    
    @IBOutlet weak var movieTitleLbl: UILabel!
    @IBOutlet weak var voteAvgLbl: UILabel!
    @IBOutlet weak var votesCntLbl: UILabel!
    
    @IBOutlet weak var imageview: UIImageView!
    
    private let imageCache = NSCache<AnyObject,AnyObject>()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Local method
    func updateCell(title: String, voteAvg: Double, voteCount: Int, backdropUrl: String) {
        self.movieTitleLbl.text = title
        self.voteAvgLbl.text = "\(voteAvg)"
        self.votesCntLbl.text = "\(voteCount)"
        
        DispatchQueue.global(qos: .background).async {
            if let cachedImage = self.imageCache.object(forKey: Constant.sharedInstance.backDropImageURL + backdropUrl as AnyObject) {
                DispatchQueue.main.async {
                    self.imageview.image = cachedImage as? UIImage
                }
            } else {
                NetworkService.getData(urlString: Constant.sharedInstance.backDropImageURL + backdropUrl) {[weak self] result in
                    guard let ws = self else {return}
                    
                    switch result {
                    case .success(let receivedData):
                        DispatchQueue.main.async {
                            if let image: UIImage = UIImage(data: receivedData) {
                                ws.imageview.image = image
                                ws.imageCache.setObject(UIImage(data: receivedData)! as AnyObject, forKey: Constant.sharedInstance.backDropImageURL + backdropUrl as AnyObject)
                            } else {
                                ws.imageview.image = UIImage(named: "backdrop")
                            }
                        }
                        
                    case .failure(let errString):
                        print(errString)
                        ws.imageview.image = UIImage(named: "backdrop")
                    }
                }
            }
        }
    }
}
