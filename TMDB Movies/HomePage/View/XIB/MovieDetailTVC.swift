//
//  MovieDetailTVC.swift
//  TMDB Movies
//
//  Created by Navindas Ghadge on 15/03/23.
//

import UIKit

class MovieDetailTVC: UITableViewCell {

    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    
    private let imageCache = NSCache<AnyObject,AnyObject>()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCell(title: String, overview: String, posterUrl: String) {
        self.titleLabel.text = title
        self.overviewLabel.text = overview
        
        DispatchQueue.global(qos: .userInteractive).async {
            if let cachedImage = self.imageCache.object(forKey: Constant.sharedInstance.posterImageURL + posterUrl as AnyObject) {
                DispatchQueue.main.async {
                    self.posterImageView.image = cachedImage as? UIImage
                }
            } else {
                NetworkService.getData(urlString: Constant.sharedInstance.posterImageURL + posterUrl) {[weak self] result in
                    guard let ws = self else {return}
                    
                    switch result {
                    case .success(let receivedData):
                        DispatchQueue.main.async {
                            if let image: UIImage = UIImage(data: receivedData) {
                                ws.posterImageView.image = image
                                ws.imageCache.setObject(UIImage(data: receivedData)! as AnyObject, forKey: Constant.sharedInstance.posterImageURL + posterUrl as AnyObject)
                            } else {
                                ws.posterImageView.image = UIImage(named: "posterblack")
                            }
                        }
                        
                    case .failure(let errString):
                        print(errString)
                        ws.posterImageView.image = UIImage(named: "posterblack")
                    }
                }
            }
        }
    }
    
}
