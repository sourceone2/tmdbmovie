//
//  MovieListDataSource.swift
//  TMDB Movies
//
//  Created by Navindas Ghadge on 14/03/23.
//

import Foundation

class MovieListDataSource {
    func getLatestMovie(completion: @escaping(_ latestMovies: LatestMovieDetailsModel?,_ status: Bool,_ message: String) -> Void) {
        //&language=en-US&page=3
        let urlString = Constant.sharedInstance.latestMovieURL + "?" + "api_key=" + Constant.sharedInstance.apiKey + "&language=en-US"
        NetworkService.getData(urlString: urlString) { result in
            switch result {
            case .success(let data):
                do {
                    let latestMovieModel = try JSONDecoder().decode(LatestMovieDetailsModel.self, from: data)
                    completion(latestMovieModel,true,"")
                } catch let caughtError {
                    completion(nil,false,caughtError.localizedDescription)
                }
                
            case .failure(let errString):
                completion(nil,false,errString)
            }
        }
    }
    
    func getPopularMovie(pageNo: Int, completion: @escaping(_ baseMovieModel: BaseMovieDataModel?,_ status: Bool,_ message: String) -> Void) {
        let urlString = Constant.sharedInstance.popularMovieURL + "?" + "api_key=" + Constant.sharedInstance.apiKey + "&language=en-US&page=\(pageNo)"
        
        NetworkService.getData(urlString: urlString) { result in
            switch result {
            case .success(let data):
                do {
                    let baseMovieModel = try JSONDecoder().decode(BaseMovieDataModel.self, from: data)
                    completion(baseMovieModel,true,"")
                } catch let caughtError {
                    completion(nil,false,caughtError.localizedDescription)
                }
                
            case .failure(let errString):
                completion(nil,false,errString)
            }
        }
    }
}
