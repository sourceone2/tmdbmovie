//
//  MovieListViewModel.swift
//  TMDB Movies
//
//  Created by Navindas Ghadge on 14/03/23.
//

import Foundation

protocol ViewModelDelegate: NSObjectProtocol {
    func willLoadData()
    func didLoadData()
    func didFail(err: String)
}

class MovieListViewModel {
    weak private var delegate: ViewModelDelegate?
    private var movieDataSource: MovieListDataSource!
    
    var baseMovieModel: BaseMovieDataModel!
    var popularMovieModel: [PopularMovieDetailsModel]!
    var latestMovieModel: LatestMovieDetailsModel!
    
    var pageNo: Int = 0
    var totalPages: Int = 0
    var totalResults: Int = 0
    
    init(delegate: ViewModelDelegate? = nil, movieDataSource: MovieListDataSource!) {
        self.delegate = delegate
        self.movieDataSource = movieDataSource
        self.popularMovieModel = []
    }
    
    func getLatestMovie() {
        self.delegate?.willLoadData()
        self.movieDataSource.getLatestMovie {[weak self] latestMovies, status, message in
            guard let ws = self else {return}
            guard let latestMovieModel = latestMovies else {
                return
            }
            ws.latestMovieModel = latestMovieModel
            _ = status ? ws.delegate?.didLoadData() : ws.delegate?.didFail(err: message)
        }
    }
    
    func getPopularMovie() {
        self.delegate?.willLoadData()
        self.movieDataSource.getPopularMovie(pageNo: pageNo == 0 ? 1 : pageNo + 1) {[weak self] baseMoviesModel, status, message in
            guard let ws = self else {return}
            guard let moviesModel = baseMoviesModel else {
                return
            }
            ws.baseMovieModel = moviesModel
            ws.pageNo = ws.baseMovieModel.page ?? 0
            
            if let popularMModel = ws.baseMovieModel.results {
                for model in popularMModel {
                    ws.popularMovieModel.append(model)
                }
            }
            
            ws.totalPages = ws.baseMovieModel.totalPages ?? 0
            ws.totalResults = ws.baseMovieModel.totalResults ?? 0
            _ = status ? ws.delegate?.didLoadData() : ws.delegate?.didFail(err: message)
        }
    }
}
