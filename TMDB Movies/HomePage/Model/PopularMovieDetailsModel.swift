//
//  MovieDetailModel.swift
//  TMDB Movies
//
//  Created by Navindas Ghadge on 14/03/23.
//

import Foundation

struct PopularMovieDetailsModel: Decodable {
    /*
     {"adult":false,
     "backdrop_path":"/jr8tSoJGj33XLgFBy6lmZhpGQNu.jpg",
     "genre_ids":[16,12,35,10751],
     "id":315162,
     "original_language":"en",
     "original_title":"Puss in Boots: The Last Wish",
     "overview":"Puss in Boots discovers that his passion for adventure has taken its toll: He has burned through eight of his nine lives, leaving him with only one life left. Puss sets out on an epic journey to find the mythical Last Wish and restore his nine lives.",
     "popularity":2531.473,
     "poster_path":"/kuf6dutpsT0vSVehic3EZIqkOBt.jpg",
     "release_date":"2022-12-07",
     "title":"Puss in Boots: The Last Wish",
     "video":false,
     "vote_average":8.4,
     "vote_count":4541}
     */
    
    var adult: Bool?
    var backdrop_path: String?
    var genre_ids : [Int]?
    var id: Int?
    var original_language: String?
    var original_title: String?
    var overview: String?
    var popularity: Double?
    var poster_path: String?
    var release_date: String?
    var title: String?
    var video: Bool?
    var vote_average: Double?
    var vote_count: Int?
    
    enum CodingKeys: CodingKey {
        case adult
        case backdrop_path
        case genre_ids
        case id
        case original_language
        case original_title
        case overview
        case popularity
        case poster_path
        case release_date
        case title
        case video
        case vote_average
        case vote_count
    }
    
    init(from decoder: Decoder) throws {
        let container = try? decoder.container(keyedBy: CodingKeys.self)
        self.adult = try? container?.decodeIfPresent(Bool.self, forKey: .adult)
        self.backdrop_path = try? container?.decodeIfPresent(String.self, forKey: .backdrop_path)
        self.genre_ids = try? container?.decodeIfPresent([Int].self, forKey: .genre_ids)
        self.id = try? container?.decodeIfPresent(Int.self, forKey: .id)
        self.original_language = try? container?.decodeIfPresent(String.self, forKey: .original_language)
        self.original_title = try? container?.decodeIfPresent(String.self, forKey: .original_title)
        self.overview = try? container?.decodeIfPresent(String.self, forKey: .overview)
        self.popularity = try? container?.decodeIfPresent(Double.self, forKey: .popularity)
        self.poster_path = try? container?.decodeIfPresent(String.self, forKey: .poster_path)
        self.release_date = try? container?.decodeIfPresent(String.self, forKey: .release_date)
        self.title = try? container?.decodeIfPresent(String.self, forKey: .title)
        self.video = try? container?.decodeIfPresent(Bool.self, forKey: .video)
        self.vote_average = try? container?.decodeIfPresent(Double.self, forKey: .vote_average)
        self.vote_count = try? container?.decodeIfPresent(Int.self, forKey: .vote_count)
    }
}
