//
//  BaseMovieDataModel.swift
//  TMDB Movies
//
//  Created by Navindas Ghadge on 14/03/23.
//

import Foundation

struct BaseMovieDataModel: Decodable {
    var page: Int?
    var results: [PopularMovieDetailsModel]?
    var totalPages: Int?
    var totalResults: Int?
    
    enum BaseMovieDataKey: String, CodingKey {
        case page = "page"
        case results = "results"
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
    
    init(from decoder: Decoder) throws {
        let values = try? decoder.container(keyedBy: BaseMovieDataKey.self)
        
        self.page = try? values?.decodeIfPresent(Int.self, forKey: .page)
        self.results = try? values?.decodeIfPresent([PopularMovieDetailsModel].self, forKey: .results)
        self.totalPages = try? values?.decodeIfPresent(Int.self, forKey: .totalPages)
        self.totalResults = try? values?.decodeIfPresent(Int.self, forKey: .totalResults)
    }
}
