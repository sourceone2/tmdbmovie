//
//  MovieGenreModel.swift
//  TMDB Movies
//
//  Created by Navindas Ghadge on 14/03/23.
//

import Foundation

struct MovieGenreModel: Decodable {
    var id: Int?
    var name: String?
    
    enum MovieGenreModel:String, CodingKey {
        case id = "id"
        case name = "name"
    }
    
    init(from decoder: Decoder) throws {
        let value = try? decoder.container(keyedBy: MovieGenreModel.self)
        
        self.id = try? value?.decodeIfPresent(Int.self, forKey: .id)
        self.name = try? value?.decodeIfPresent(String.self, forKey: .name)
    }
}
