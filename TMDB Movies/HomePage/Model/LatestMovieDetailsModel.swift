//
//  LatestMovieDetailsModel.swift
//  TMDB Movies
//
//  Created by Navindas Ghadge on 14/03/23.
//

import Foundation

struct LatestMovieDetailsModel: Decodable {
    /*
     {"adult":true,
     "backdrop_path":null,
     "belongs_to_collection":null,
     "budget":0,
     "genres":[],
     "homepage":"",
     "id":1099944,
     "imdb_id":null,
     "original_language":"en",
     "original_title":"OBSESSION",
     "overview":"zara whites",
     "popularity":0.0,
     "poster_path":"/3kZ38knQKXHItbRImlh2Nyfatxp.jpg",
     "production_companies":[],
     "production_countries":[],
     "release_date":"1991-01-01",
     "revenue":0,
     "runtime":0,
     "spoken_languages":[],
     "status":"Released",
     "tagline":"",
     "title":"OBSESSION",
     "video":true,
     "vote_average":0.0,
     "vote_count":0}
     */
    
    /*
     {"adult":false,
     "backdrop_path":null,
     "belongs_to_collection":null,
     "budget":0,
     "genres":[{"id":10402,"name":"Music"}],
     "homepage":"https://www.youtube.com/watch?v=h96MGcsi7GQ",
     "id":1099976,
     "imdb_id":null,
     "original_language":"en",
     "original_title":"Lee Burridge at Omnia Bali in Indonesia for Cercle",
     "overview":"Lee Burridge playing an exclusive set at Omnia Bali for Cercle.",
     "popularity":0.0,
     "poster_path":null,
     "production_companies":[{"id":168111,"logo_path":"/9UdjQWnPDQpXF1TpKDsq8Wtri9d.png","name":"Cercle","origin_country":"FR"}],
     "production_countries":[{"iso_3166_1":"FR","name":"France"},{"iso_3166_1":"ID","name":"Indonesia"}],
     "release_date":"2020-07-02",
     "revenue":0,
     "runtime":106,
     "spoken_languages":[],
     "status":"Released",
     "tagline":"",
     "title":"Lee Burridge at Omnia Bali in Indonesia for Cercle",
     "video":false,
     "vote_average":0.0,
     "vote_count":0}
     */
    
    var adult: Bool?
    var backdrop_path: String?
    var belongs_to_collection: String?
    var budget: Int?
    var genres: [MovieGenreModel]?
    var homepage: String?
    var id: Int?
    var imdb_id: Int?
    var original_language: String?
    var original_title: String?
    var overview: String?
    var popularity: Double?
    var poster_path: String?
    var production_companies: [ProductionCompanyModel]?
    var release_date: String?
    var revenue: Int?
    var runtime: Int?
    var status: String?
    var tagline: String?
    var title: String?
    var video: Bool?
    var vote_average: Double?
    var vote_count: Int?
    
    enum LatestMovieKey: String, CodingKey {
        case adult = "adult"
        case backdrop_path = "backdrop_path"
        case belongs_to_collection = "belongs_to_collection"
        case budget = "budget"
        case genres = "genres"
        case homepage = "homepage"
        case id = "id"
        case imdb_id = "imdb_id"
        case original_language = "original_language"
        case original_title = "original_title"
        case overview = "overview"
        case popularity = "popularity"
        case poster_path = "poster_path"
        case production_companies = "production_companies"
        case release_date = "release_date"
        case revenue = "revenue"
        case runtime = "runtime"
        case status = "status"
        case tagline = "tagline"
        case title = "title"
        case video = "video"
        case vote_average = "vote_average"
        case vote_count = "vote_count"
    }
    
    init(from decoder: Decoder) throws {
        let values = try? decoder.container(keyedBy: LatestMovieKey.self)
        self.adult = try? values?.decodeIfPresent(Bool.self, forKey: .adult)
        self.backdrop_path = try? values?.decodeIfPresent(String.self, forKey: .backdrop_path)
        self.belongs_to_collection = try? values?.decodeIfPresent(String.self, forKey: .belongs_to_collection)
        self.budget = try? values?.decodeIfPresent(Int.self, forKey: .budget)
        self.genres = try? values?.decodeIfPresent([MovieGenreModel].self, forKey: .genres)
        self.homepage = try? values?.decodeIfPresent(String.self, forKey: .homepage)
        self.id = try? values?.decodeIfPresent(Int.self, forKey: .id)
        self.imdb_id = try? values?.decodeIfPresent(Int.self, forKey: .imdb_id)
        self.original_language = try? values?.decodeIfPresent(String.self, forKey: .original_language)
        self.original_title = try? values?.decodeIfPresent(String.self, forKey: .original_title)
        self.overview = try? values?.decodeIfPresent(String.self, forKey: .overview)
        self.popularity = try? values?.decodeIfPresent(Double.self, forKey: .popularity)
        self.poster_path = try? values?.decodeIfPresent(String.self, forKey: .poster_path)
        self.production_companies = try? values?.decodeIfPresent([ProductionCompanyModel].self, forKey: .production_companies)
        self.release_date = try? values?.decodeIfPresent(String.self, forKey: .release_date)
        self.revenue = try? values?.decodeIfPresent(Int.self, forKey: .revenue)
        self.runtime = try? values?.decodeIfPresent(Int.self, forKey: .runtime)
        self.status = try? values?.decodeIfPresent(String.self, forKey: .status)
        self.tagline = try? values?.decodeIfPresent(String.self, forKey: .tagline)
        self.title = try? values?.decodeIfPresent(String.self, forKey: .title)
        self.video = try? values?.decodeIfPresent(Bool.self, forKey: .video)
        self.vote_average = try? values?.decodeIfPresent(Double.self, forKey: .vote_average)
        self.vote_count = try? values?.decodeIfPresent(Int.self, forKey: .vote_count)
    }
}
