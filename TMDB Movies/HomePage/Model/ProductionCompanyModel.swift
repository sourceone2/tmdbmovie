//
//  ProductionCompanyModel.swift
//  TMDB Movies
//
//  Created by Navindas Ghadge on 14/03/23.
//

import Foundation

struct ProductionCompanyModel: Decodable {
    /*
     {"id":168111,
     "logo_path":"/9UdjQWnPDQpXF1TpKDsq8Wtri9d.png",
     "name":"Cercle",
     "origin_country":"FR"}
     */
    
    var id: Int?
    var logo_path: String?
    var name: String?
    var origin_country: String?
    
    enum ProductionCompanyKey: String, CodingKey {
        case id = "id"
        case logo_path = "logo_path"
        case name = "name"
        case origin_country = "origin_country"
    }
    
    init(from decoder: Decoder) throws {
        let values = try? decoder.container(keyedBy: ProductionCompanyKey.self)
        self.id = try? values?.decodeIfPresent(Int.self, forKey: .id)
        self.logo_path = try? values?.decodeIfPresent(String.self, forKey: .logo_path)
        self.name = try? values?.decodeIfPresent(String.self, forKey: .name)
        self.origin_country = try? values?.decodeIfPresent(String.self, forKey: .origin_country)
    }
}
